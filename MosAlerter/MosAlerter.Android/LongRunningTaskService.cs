﻿using Android.App;
using Android.OS;
using MosAlerter.Models;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Android.Content;
using System;
using Android.Util;

[Service]
public class LongRunningTaskService : Service
{
    CancellationTokenSource _cts;
    private int i=0;

    public override IBinder OnBind(Intent intent)
    {
        return null;
    }

    public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
    {
        // _cts = new CancellationTokenSource();
        // var t = new Thread(() => {
        //     Log.Debug("DemoService", "Doing work");
        //     Thread.Sleep(5000);
        //     Log.Debug("DemoService", "Work complete");
        //     var counter = new RestServiceRunner(MosAlerter.App.Instance.RestServiceNews);
        //     counter.Run(_cts.Token).Wait();
        //     StopSelf();
        // }
        //);
        // t.Start();
        // return StartCommandResult.Sticky;
        _cts = new CancellationTokenSource();

        Task.Run(() =>
        {
            try
            {
                //i += 1;
                //var notification = new LocalNotifications.Plugin.Abstractions.LocalNotification
                //{
                //    Text = "Hello from Plugin" + i.ToString(),
                //    Title = "HI! Notification Plugin" + i.ToString(),
                //    Id = 2,
                //    NotifyTime = DateTime.Now

                //};

                //var notifier = LocalNotifications.Plugin.CrossLocalNotifications.CreateLocalNotifier();
                //notifier.Notify(notification);
                //Console.WriteLine("Sending a notification no" + i);

                //INVOKE THE SHARED CODE
                var counter = new RestServiceRunner(new MosAlerter.Services.RestServiceNews());
                counter.Run(_cts.Token).Wait();
            }
            catch (Android.Accounts.OperationCanceledException)
            {
            }
            finally
            {
                if (_cts.IsCancellationRequested)
                {
                    //var message = new CancelledMessage();
                    //Device.BeginInvokeOnMainThread(
                    //    () => MessagingCenter.Send(message, "CancelledMessage")
                    //);
                }
            }

        }, _cts.Token);
        Console.WriteLine("RETURN STICKY");
        return StartCommandResult.Sticky;
    }

    public override void OnDestroy()
    {
        if (_cts != null)
        {
            _cts.Token.ThrowIfCancellationRequested();
            Console.WriteLine(" _cts.Cancel()!!");
            _cts.Cancel();
        }
        base.OnDestroy();
    }

    //public override IBinder OnBind(Intent intent)
    //{
    //    throw new NotImplementedException();
    //}
}

internal class CancelledMessage
{
    public CancelledMessage()
    {
    }
}