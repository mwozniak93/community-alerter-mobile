﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
//using LocalNotifications.Plugin;
//using LocalNotifications.Plugin.Abstractions;
using MosAlerter.Models;
using Xamarin.Forms;
using Android.Content;
using MosAlerter.Droid.Services;

namespace MosAlerter.Droid
{
	[Activity (Label = "MosAlerter", Icon = "@drawable/icon", Theme="@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar; 

			base.OnCreate (bundle);
            
            global::Xamarin.Forms.Forms.Init (this, bundle);
			LoadApplication (new MosAlerter.App ());

            //AndroidNotificationManager notification = new AndroidNotificationManager(this);
            //notification.SendNotification();

            //var notification = new LocalNotification
            //{
            //    Text = "Hello from Plugin",
            //    Title = "Notification Plugin",
            //    Id = 2,
            //    NotifyTime = DateTime.Now
            //};

            //var notifier = CrossLocalNotifications.CreateLocalNotifier();
            //notifier.Notify(notification);
            MessagingCenter.Subscribe<StartLongRunningTaskMessage>(this, "StartLongRunningTaskMessage", message => {
                var intent = new Intent(this, typeof(LongRunningTaskService));
                StartService(intent);
                Console.WriteLine("StartService LongRunningTaskService");
            });

            MessagingCenter.Subscribe<StopLongRunningTaskMessage>(this, "StopLongRunningTaskMessage", message => {
                var intent = new Intent(this, typeof(LongRunningTaskService));
                StopService(intent);
            });


        }
	}
}

