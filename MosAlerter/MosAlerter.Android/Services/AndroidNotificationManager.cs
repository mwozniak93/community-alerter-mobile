using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MosAlerter.Models;
using Android.Support.V4.App;
using TaskStackBuilder = Android.Support.V4.App.TaskStackBuilder;
using MosAlerter.Droid.Services;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidNotificationManager))]
namespace MosAlerter.Droid.Services
{
    public class AndroidNotificationManager : Java.Lang.Object, INotification
    {
   
        public AndroidNotificationManager() { }
 
        public void SendNotification(string title, string message)
        {
            var ctx = Android.App.Application.Context;
            Intent intent = new Intent(ctx, typeof(MainActivity));
            const int pendingIntentId = 0;
            PendingIntent pendingIntent = PendingIntent.GetActivity(ctx, pendingIntentId, intent, PendingIntentFlags.OneShot);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx)
    .SetContentTitle(title)
    .SetContentIntent(pendingIntent)
    .SetContentText(message)
    .SetDefaults((int)NotificationDefaults.Vibrate)
    .SetSmallIcon(Resource.Drawable.notification_template_icon_bg);

            // Build the notification:
            Notification notification = builder.Build();

            // Get the notification manager:
            NotificationManager notificationManager = (NotificationManager)ctx.GetSystemService(Context.NotificationService);
            const int notificationId = 0;
            notificationManager.Notify(notificationId, builder.Build());

            // Publish the notification:
           // const int notificationId = 0;
            //notificationManager.Notify(notificationId, notification);
            //throw new NotImplementedException();
        }
    }
}