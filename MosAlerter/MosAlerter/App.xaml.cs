﻿using MosAlerter.Models;
using MosAlerter.Services;
using MosAlerter.ViewModels;
using MosAlerter.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace MosAlerter
{
	public partial class App : Application
	{
        public static App Instance { get; set; }
         public RestServiceNews RestServiceNews { get; set; }
		public App ()
		{
			InitializeComponent();
            Instance = this;
            
           
            NewsViewModel oNewsVM = new NewsViewModel();
            RestServiceNews = new RestServiceNews(oNewsVM);
            TabsContainer newTabsContainer = new TabsContainer();
            newTabsContainer.Children.Add(new NewsView(oNewsVM, RestServiceNews));
            MainPage = newTabsContainer;
              
		}

		protected override void OnStart ()
		{
            var message = new StartLongRunningTaskMessage();
            MessagingCenter.Send(message, "StartLongRunningTaskMessage");
           
            // Handle when your app starts
        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
