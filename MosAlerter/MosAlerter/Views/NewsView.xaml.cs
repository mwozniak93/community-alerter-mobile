﻿using Android.Util;
using MosAlerter.Models;
using MosAlerter.Services;
using MosAlerter.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MosAlerter.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewsView : ContentPage
    {
        NewsViewModel oNewsVM { get; set; }
        RestServiceNews newsRest;
        // public ObservableCollection<News> NewsItems;
        public NewsView(NewsViewModel newsVM, RestServiceNews rest)
        {
            InitializeComponent();
            oNewsVM = newsVM;
            BindingContext = oNewsVM;
            //oNewsVM.NewsItems.Add(new News() { Title = "Sattus is OK 200!" });
            // BindingContext = new NewsViewModel() { Status = "200 OK" };
            oNewsVM.Status = "LOADING...";
            //BindingContext = oNewsVM;
            //oNewsVM.Init();
            Task.Run(async () =>
            {
                await newsRest.GetDataAsync();
                Device.BeginInvokeOnMainThread(() =>
                {
                    //BindingContext = oNewsVM;
                });

            });
           
        }

        public NewsView()
        {
        }

        void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
            => ((ListView)sender).SelectedItem = null;

        //async void Handle_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        //{
        //    if (e.SelectedItem == null)
        //        return;



        //    await DisplayAlert("Selected", e.SelectedItem.ToString(), "OK");

        //    //Deselect Item
        //    ((ListView)sender).SelectedItem = null;
        //}
        private ViewCell lastCell;
        private Label lastLabel;
        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (lastCell != null && lastLabel!=null)
            {
                Grid.SetRow(lastLabel, 1);
                lastCell.ForceUpdateSize();
            }
            //Label label = (Label)sender;
            ViewCell viewCell = (ViewCell)sender;
            Label label = viewCell.FindByName<Label>("DetailLabel");
            Grid.SetRow(label, 2);
            viewCell.ForceUpdateSize();
            lastCell = viewCell;
            lastLabel = label;
        }



    }

}
