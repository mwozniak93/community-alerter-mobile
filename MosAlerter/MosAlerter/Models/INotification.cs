﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MosAlerter.Models
{
    public interface INotification
    {

         void SendNotification(string title, string message);

    }
}
