﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MosAlerter.Models
{
   public class News
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

    }
}
