﻿using MosAlerter.Models;
using MosAlerter.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

public class RestServiceRunner
{
    long i;
    private RestServiceNews restServiceNews;
    //IRest<RestServiceNews> _restService;

    public RestServiceRunner()
    {
    }

    public RestServiceRunner(RestServiceNews restServiceNews)
    {
        this.restServiceNews = restServiceNews;
    }


    public async Task Run(CancellationToken token)
    {
        await Task.Run(async () =>
        {

            token.ThrowIfCancellationRequested();

            await Task.Delay(10000);
            await restServiceNews.CheckNewInfoAsync();
            var message = new TickedMessage
            {
                Message = i.ToString()
            };
            this.Run(token).Wait();
        }, token);
    }

}