﻿using MosAlerter.Models;
using MosAlerter.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MosAlerter.ViewModels
{
    public  class NewsViewModel:INotifyPropertyChanged
    {
        // RestServiceNews restServiceNews;
        public NewsViewModel() {
 
            Status = "Waiting...";
            //Task.Run(async () =>
            //{

            //    //List<News> news =  await restServiceNews.GetDataAsync();
            //    NewsItems =  new ObservableCollection<News>(news);
            //    Device.BeginInvokeOnMainThread(() =>
            //    {
            //        //BindingContext = oNewsVM;
            //    });

            //});
            //_restServiceNews.GetDataAsync();
            //_newsManager = newsManager;
            //Init();

            //NewsItems = 
        }


        // NewsManager _newsManager;
        private ObservableCollection<News> _newsItems = new ObservableCollection<News>() { new News() { Title="TestTitle"} };
        public ObservableCollection<News> NewsItems
        {
            get { return _newsItems; }
            set
            {
                _newsItems = value;
                OnPropertyChanged("NewsItems");
            }
        }
        private string _status;
        public string Status {
            get {
                return _status;
            }
                set

            {
                _status= value;
                OnPropertyChanged("Status");
            }
                }
        //public async void Init() {
        //    List<News> news;
        //    //news = await _newsManager.GetNewsAsync();
        //    NewsItems = new ObservableCollection<News>(news);
        //}



        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName]string propertyName = "") =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

    }
    }
