﻿using MosAlerter.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MosAlerter.Services
{
   public  interface IRest<T>
    {
        string Url { get; }
         T ViewModel { get; set; }
           Task<List<News>> GetDataAsync();
    }
}
