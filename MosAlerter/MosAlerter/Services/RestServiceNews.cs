﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

using MosAlerter.Models;
using Newtonsoft.Json;
using MosAlerter.ViewModels;

namespace MosAlerter.Services
{
    public class RestServiceNews : IRest<NewsViewModel>
    {
        HttpClient client;
        public List<News> NewsItems { get; set; }
        public async Task<List<News>> GetDataAsync()
        {

            NewsItems = new List<News>();
            // RestUrl =http://demo1841110.mockable.io/
            var uri = new Uri(string.Format(Url, string.Empty));

            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    NewsItems = JsonConvert.DeserializeObject<List<News>>(content);

                        ViewModel.NewsItems = new System.Collections.ObjectModel.ObservableCollection<News>(NewsItems);
                        ViewModel.Status = "Loaded!";
                    
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }

            return NewsItems;
        }

        public async Task CheckNewInfoAsync() {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            var uri = new Uri(string.Format(Url, string.Empty));
            List<News> newsItems = new List<News>();
            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    newsItems = JsonConvert.DeserializeObject<List<News>>(content);
                    //CrossPushNotification.Current
                    //Droid.Services.AndroidNotificationManager oNotify = new Droid.Services.AndroidNotificationManager();
                    //oNotify.SendNotification("Mosina alert!", newsItems[0].Content);
                    //Xamarin.Forms.Forms.Init();

                    //DependencyService.Get<INotification>().SendNotification("Mosina alert!", newsItems[0].Content);
                    var notification = new LocalNotifications.Plugin.Abstractions.LocalNotification
                    {
                        Text = "New Info from Mosina!",
                        Title = newsItems[0].Title,
                        Id = 2,
                        NotifyTime = DateTime.Now


                    };
                    var notifier = LocalNotifications.Plugin.CrossLocalNotifications.CreateLocalNotifier();
                    notifier.Notify(notification);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }

        }


        public string Url
        {
            get
            {
                return "http://demo1841110.mockable.io/";
            }
        }
        private NewsViewModel _viewModel;
        public NewsViewModel ViewModel
        {
            get
            {
                return _viewModel;
            }

            set
            {
                _viewModel = value;
            }
        }


        public RestServiceNews() {
            //client = new HttpClient();
            //client.MaxResponseContentBufferSize = 256000;
            //Task.Run(async () =>
            //{
            //    await CheckNewInfoAsync();
            //});
        }
        public RestServiceNews(NewsViewModel viewModel)
        {
            ViewModel = viewModel;
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            Task.Run(async () =>
            {
                await GetDataAsync();
            });
        }


    }
}
